from django.db import connection
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import ChatMessage, Connection
import boto3

# Create your views here.

@csrf_exempt
def test(request):

    return JsonResponse({'message': 'hello Daud'}, status=200)


def _parse_body(body):
    body_unicode = body.decode('utf-8')

    return json.loads(body_unicode)


@csrf_exempt
def connect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    Connection.objects.create(connection_id=connection_id)

    return JsonResponse({'message': 'connect successfully'}, status=200)

@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    Connection.objects.get(connection_id=connection_id).delete() 

    return JsonResponse({'message': 'disconnect successfully'}, status=200, safe=False)


@csrf_exempt
def _send_to_connection(connection_id, data):

    gatewayapi=boto3.client('apigatewaymanagementapi', endpoint_url="https://sb1k7vpnd4.execute-api.eu-central-1.amazonaws.com/test",region_name="eu-central-1", aws_access_key_id="AKIAVIDCOTII5SH3ME4K",aws_secret_access_key="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", )
    response = gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8'))
    return response



@csrf_exempt
def send_message(request):
    print("send message was successfull")
    body = _parse_body(request.body)
    dictionary_body = dict(body)
    username = dictionary_body['body']['username']
    timestamp = dictionary_body['body']['timestamp']
    content = dictionary_body['body']['content']
    ChatMessage.objects.create(username=username, timestamp=timestamp, message=content)
    connections = Connection.objects.all()
    messages = {
        "username":username,
        "timestamp":timestamp,
        "messages":content
    }
    data = {"messages":[messages]} 
    for connection in connections:
        _send_to_connection(connection.connection_id, data)
    return JsonResponse({"message":"successfully sent"}, status=200, safe=False)


@csrf_exempt
def get_message(request):
    print("Get message was successfull")
    connections = Connection.objects.all()
    all_message = ChatMessage.objects.all()
    response_array=[]
    for message in all_message:
        messages = {
        "username":message.username,
        "timestamp":message.timestamp,
        "messages":message.message
        }
        response_array.append(messages)
    
    data = {"messages":response_array} 
    for connection in connections:
        _send_to_connection(connection.connection_id, data)
    return JsonResponse({"message":"successfully sent"}, status=200, safe=False)



# WebSocket URL: wss://sb1k7vpnd4.execute-api.eu-central-1.amazonaws.com/test
# Connection URL: https://sb1k7vpnd4.execute-api.eu-central-1.amazonaws.com/test/@connections

